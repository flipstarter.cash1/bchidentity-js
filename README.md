# bchidentity

bchidentity is a JavaScript library that lets you authenticate using the
[BCH Identity Protocol](https://gitlab.com/bitcoinunlimited/bchidentity-js/-/blob/master/identity_protocol.md).

![logo](https://gitlab.com/bitcoinunlimited/bchidentity-js/-/raw/master/contrib/logo-256x256.png)

- **[NPM package](https://www.npmjs.com/package/@bitcoinunlimited/bchidentity)**
- **[Code reference](https://bitcoinunlimited.gitlab.io/bchidentity-js)**

## Example


```javascript
const bchidentity = require("bchidentity");
(async () => {

    // Private key to sign authentication challenge with
    const wif = 'L4vmKsStbQaCvaKPnCzdRArZgdAxTqVx8vjMGLW5nHtWdRguiRi1';

    // The bchidentity login offer
    const offer = "bchidentity://example.org:443/login?op=login&chal=ci7P10BiPlL7B1QWa&cookie=fBAMYnHSwa";

    bchidentity.identify(offer, wif)
        .then(([status, response]) => {
            console.log("OK: ", status, response);
        })
        .catch(([status, response]) => {
            console.log("Error: ", status, response);
        });
})();
```

## Client example

Examples includes an example authenticating client. The private key
authenticated with is deterministically generated from the `--user` parameter.

Example:
```
$ npm run cli -- "bchidentity://example.org:443/login?op=login&chal=ci7P10BiPlL7B1QWa&cookie=fBAMYnHSwa" --user bob

Authenticating to bchidentity://example.org:443/login?op=login&chal=ci7P10BiPlL7B1QWa&cookie=fBAMYnHSwa with address 19vcfWqUTxHvSfPfBqE54bWVGS5R5R7LCR and private key KzX4C7Hrk2sXzVTGjGqekakwXLc3NZ6G6Rta4JBduMaF49hLe9sE
```

### Demo

Go to <https://voter.cash/#/login>, copy the `bchidentity://` url and run `npm run cli -- <url>`.

## License

This project is licensed under the MIT License.
