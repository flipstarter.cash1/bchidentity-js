import util from "util";
const url = require("url");
const querystring = require("querystring");
const bitcoin = require("bitcoinjs-lib");
const bitcoinMessage = require("bitcoinjs-message");
import assert from "assert";
import axios from "axios";
import bchaddr from "bchaddrjs";

/**
 * Data provided by parsing a bchidentity:// login offer
 */
interface ILoginOffer {
    /**
     * An arbitrary string chosen by the server that is unique for every login
     * and contains sufficient entropy to make repeats incredibly unlikely. The
     * challenge string MUST only contain ASCII alphanumeric characters (A-Z,
     * a-z, 0-9, and _)
     */
    chal: string;
    /**
     * Arbitrary data chosen by the server that will be sent back to the
     * server during the login response. For web sites, this is often the
     * originating session identifier since the login response will likely
     * arrive in a different session.
     */
    cookie: string;
    /**
     * Domain name of the site to log into (e.g. "www.bitcoinunlimited.net")
     */
    domain: string;
    /**
     * This is the operation specified in the request URI. Currently this must
     * equal "login".
     */
    op: string;
    /**
     * Where to send the response (e.g. "/login/auto")
     */
    path: string;
    /**
     * Defaults to 80. If set to 443, a https response is expected.
     */
    port: number;
}

/**
 * Parses and validates a bchidentity:// login offer, returning the parsed
 * values.
 *
 * @param uri - The bchidentity:// URI
 * @param strict? - Throw on invalid URI (default: true)
 */
export function parse_offer(uri: string, strict?: boolean): ILoginOffer {
    if (strict === undefined) {
        strict = true;
    }

    const u = url.parse(uri);
    if (strict && u.protocol !== "bchidentity:") {
        throw Error("'bchidentity://' protocol prefix missing");
    }

    const domain = u.hostname;
    if (strict && domain === "") {
        throw Error("'domain' missing");
    }
    let port = u.port || 80;
    // If string, convert to int
    port = +port;

    const query = querystring.parse(u.query);
    const getVar = (name: string) => {
        const v = query[name];
        if (v === undefined) {
            if (strict) {
                throw new Error(util.format("'%s' missing", name));
            }
            return null;
        }
        return v;
    };

    return {
        chal: getVar("chal"),
        cookie: getVar("cookie"),
        domain,
        op: getVar("op"),
        path: u.pathname,
        port,
    } as ILoginOffer;
}

/**
 * Given that signed offer, returns a login response URL.
 *
 * @param domain - The domain from a login offer
 * @param port - The port from a login offer
 * @param path - The path from a login offer
 * @param cookie - The cookie from the login offer (if any)
 * @param addr - Bitcoin address for authenticating
 * @param signature - The login offer challenge signed with Bitcoin address
 */
export function create_response_url(
    domain: string, port: number, path: string, cookie: string | null,
    addr: string, signature: Buffer): string {

    const protocol = port === 443 ? "https" : "http";
    if (port !== 80 && port !== 443) {
        domain = util.format("%s:%s", domain, port);
    }

    if (cookie === null) {
        cookie = "";
    } else {
        cookie = util.format("&cookie=%s", querystring.escape(cookie as string));
    }

    let params = querystring.stringify({
        addr,
        op: "login",
        sig: signature.toString("base64"),
    });
    params += cookie;

    return util.format("%s://%s%s?%s",
        protocol,
        domain,
        path,
        params);
}

/**
 * Signs a login offer challenge with given private key (WIF). Returns
 * signature and Bitcoin address associated with the private key.
 *
 * @param wif - Private key (WIF encoded)
 * @param challenge - Login offer challenge
 */
export function sign_login_challenge(wif: string, challenge: string): [Buffer, string] {
    const keyPair = bitcoin.ECPair.fromWIF(wif);
    const privkey = keyPair.privateKey;
    const signature = bitcoinMessage.sign(
        challenge, privkey, keyPair.compressed);
    let { address } = bitcoin.payments.p2pkh({
        pubkey: keyPair.publicKey });
    assert(address !== undefined);
    address = address as string;
    assert(bitcoinMessage.verify(challenge, address, signature));
    return [signature, bchaddr.toCashAddress(address)];
}

/**
 * Respond to a login offer response.
 *
 * Parses a bchidentity string, signs the challenge with the private key and
 * submits a response.
 *
 * @param offer - The bchidentity:// login offer string
 * @param wif - The private key (WIF encoded)
 * @returns reponse Tuple of HTTP code and server response
 */
export async function identify(
    offer: string, wif: string): Promise<[number, string]> {
    const parsed = parse_offer(offer);
    const signed = sign_login_challenge(
        wif, parsed.chal);

    const responseURL: string = create_response_url(
        parsed.domain,
        parsed.port,
        parsed.path,
        parsed.cookie,
        signed[1], signed[0]);

    try {
        const response = await axios.get(responseURL);
        return Promise.resolve([
            response.status,
            response.data]);
    } catch (error) {
        const e = error as any;
        return Promise.reject([
            e.response.status,
            e.response.data]);
    }
}
